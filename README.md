# Tensorflow Study



## Overview

This is a project with the models I created to prepare the tensorflow certification exam.

## BBC News

- [ ] Dataset from [Kaggle](https://www.kaggle.com/datasets/hgultekin/bbcnewsarchive)
- [ ] Natural Language Processing
- [ ] Classification of news in business, entertainment, politics, sport, tech

## Daily climante

- [ ] Dataset from [Kaggle](https://www.kaggle.com/datasets/sumanthvrao/daily-climate-time-series-data)
- [ ] Time Series
- [ ] Forecasting weather

## Floral diversity

- [ ] Dataset from [Kaggle](https://www.kaggle.com/datasets/mansi0123/floral-diversity-a-collection-of-beautiful-blooms)
- [ ] CNNs
- [ ] Classification of images in daisy, dandelion, roses, sunflowers, tulips

## Oranges vs grapefruit

- [ ] Dataset from [Kaggle](https://www.kaggle.com/datasets/joshmcadams/oranges-vs-grapefruit)
- [ ] DNNs
- [ ] Binary classification. Orange or not

## Plant pathology

- [ ] Dataset from [Kaggle](https://www.kaggle.com/c/plant-pathology-2020-fgvc7)
- [ ] CNN with CNN labels
- [ ] Measuring health of leaves using a percentage of truth for healthy, rust, scab or combination of sickneses

## Romeo and Juliet

- [ ] [Romeo and Juliet txt file](https://folger-main-site-assets.s3.amazonaws.com/uploads/2022/11/romeo-and-juliet_TXT_FolgerShakespeare.txt)
- [ ] Natural Language Processing
- [ ] Generation of text in the style of Romeo and Juliet

## Sentiment analysis

- [ ] Sentiment140 dataset from [Kaggle](https://www.kaggle.com/datasets/kazanova/sentiment140)
- [ ] Natural Language Processing
- [ ] Classification of tweets in positive or negative

## Whats cooking

- [ ] Dataset from [Kaggle](https://www.kaggle.com/c/whats-cooking-kernels-only)
- [ ] Natural Language Processing
- [ ] Classification of recipies in styles

