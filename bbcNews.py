import pandas as pd
import nltk  # For stopword removal
import re
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.keras import Model
from tensorflow.keras.layers import TextVectorization, StringLookup, Input, Embedding, Bidirectional, LSTM, Concatenate, \
    Dense, GlobalAveragePooling1D, concatenate, Dropout
import numpy as np

# Download stopwords if not already available
nltk.download('punkt')
nltk.download('stopwords')


class G:
    EPOCHS = 10
    MAX_LEN_TITLE = 10
    MAX_LEN_CONTENT = 1000
    EMBEDDINGS_DIM = 100
    VALIDATION_PERCENTAGE = 0.2
    MAX_WORDS = 300
    EMBEDDING_FILE = 'data/glove.6B.100d.txt'
    DATASET_FILE = 'data/bbc-news-data.csv'


def preprocess_string(string):
    remove_regex = r'[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']'
    s = tf.strings.regex_replace(string, remove_regex, '')

    # remove stock market tickers like $GE
    s = tf.strings.regex_replace(s, b'\$\w*', b'')
    # remove old style retweet text "RT"
    s = tf.strings.regex_replace(s, b'^RT[\s]+', b'')

    # lower case
    s = tf.strings.lower(s)
    # remove digits
    s = tf.strings.regex_replace(s, b'[0-9]+', b'')
    # remove hyperlinks
    s = tf.strings.regex_replace(s, b'https?://[^\s\n\r]+', b'')

    return s


# Define a preprocessing function
def preprocess_text(text):
    # Convert text to lowercase
    text = text.lower()

    # Remove punctuation and special characters
    text = re.sub(r'[^\w\s]', '', text)

    # Tokenize the text into words
    words = nltk.word_tokenize(text)

    # Remove stop words
    stop_words = nltk.corpus.stopwords.words('english')
    words = [word for word in words if word not in stop_words]

    # Join the words back into a preprocessed text string
    preprocessed_text = ' '.join(words)

    return preprocessed_text


def adapt_precompute_layers(dataset, output_sequence_length):
    vectorizer = TextVectorization(output_sequence_length=output_sequence_length)
    vectorizer.adapt(dataset)

    return vectorizer


def load_embeddings():
    embeddings_index = {}
    with open(G.EMBEDDING_FILE, encoding="utf-8") as f:
        for line in f:
            word, coefs = line.split(maxsplit=1)
            coefs = np.fromstring(coefs, "f", sep=" ")
            embeddings_index[word] = coefs

    print("Found %s word vectors." % len(embeddings_index))

    return embeddings_index


def generate_embeddings_matrix(vectorizer, embeddings):
    voc = vectorizer.get_vocabulary()
    word_index = dict(zip(voc, range(len(voc))))

    num_tokens = len(voc)
    hits = 0
    misses = 0

    # Prepare embedding matrix
    embedding_matrix = np.zeros((num_tokens, G.EMBEDDINGS_DIM))
    for word, i in word_index.items():
        embedding_vector = embeddings.get(word)
        if embedding_vector is not None:
            # Words not found in embedding index will be all-zeros.
            # This includes the representation for "padding" and "OOV"
            embedding_matrix[i] = embedding_vector
            hits += 1
        else:
            misses += 1
    print("Converted %d words (%d misses)" % (hits, misses))

    return embedding_matrix


def generate_model(title_vectorizer, title_embedding_matrix, content_vectorizer, content_embedding_matrix, num_labels):
    title_input = Input(shape=(1,), dtype=tf.string, name='title')
    content_input = Input(shape=(1,), dtype=tf.string, name='content')

    title_vectors = title_vectorizer(title_input)
    title_embedding = (Embedding(len(title_vectorizer.get_vocabulary()),
                                 G.EMBEDDINGS_DIM,
                                 weights=[title_embedding_matrix])
                       (title_vectors))
    title_embedding = Dropout(0.3)(title_embedding)

    content_vectors = content_vectorizer(content_input)
    content_embedding = (Embedding(len(content_vectorizer.get_vocabulary()),
                                   G.EMBEDDINGS_DIM,
                                   weights=[content_embedding_matrix])
                         (content_vectors))
    content_embedding = Dropout(0.3)(content_embedding)

    title_pooling = GlobalAveragePooling1D()(title_embedding)
    content_pooling = GlobalAveragePooling1D()(content_embedding)

    combined = concatenate([title_pooling, content_pooling], 1)

    l1 = Dense(5, activation='relu')(combined)
    output = Dense(num_labels, activation='softmax')(l1)
    model = Model(inputs=[title_input, content_input], outputs=output)

    model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics='accuracy')
    return model


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Load the dataset
    df = pd.read_csv(G.DATASET_FILE, sep=None)
    # Apply the preprocessing function to the title and content columns
    df['title'] = df['title'].apply(preprocess_text)
    df['content'] = df['content'].apply(preprocess_text)

    # Split the dataframe into train and test sets
    X_train, X_test, y_train, y_test = train_test_split(
        df[['title', 'content']],  # Features
        df['category'],  # Labels
        test_size=G.VALIDATION_PERCENTAGE,
        random_state=42  # Set a random seed for reproducibility
    )

    # Define TextVectorization layer for titles
    title_vectorizer = adapt_precompute_layers(X_train['title'], output_sequence_length=10)

    # Define TextVectorization layer for content
    content_vectorizer = adapt_precompute_layers(X_train['content'], output_sequence_length=1000)

    embeddings = load_embeddings()
    title_embedding_matrix = generate_embeddings_matrix(title_vectorizer, embeddings)
    content_embedding_matrix = generate_embeddings_matrix(content_vectorizer, embeddings)

    labels_vocabulary = ['business', 'entertainment', 'politics', 'sport', 'tech']
    labels_tokenizer = StringLookup(vocabulary=labels_vocabulary, num_oov_indices=0)

    y_train = labels_tokenizer(y_train)
    y_test = labels_tokenizer(y_test)

    model = generate_model(title_vectorizer,
                           title_embedding_matrix,
                           content_vectorizer,
                           content_embedding_matrix,
                           len(labels_vocabulary))
    model.summary()

    history = model.fit(dict(X_train), y_train, epochs=G.EPOCHS, validation_data=(dict(X_test), y_test))
