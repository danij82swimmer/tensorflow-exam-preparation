import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import LSTM, Dense, Normalization, StringLookup
import matplotlib.pyplot as plt
import random


class G:
    EPOCHS = 10
    VALIDATION_PERCENTAGE = 0.2
    DATASET_FILE = 'data/DailyDelhiClimateTrain.csv'
    FEATURES = ['meantemp', 'humidity', 'wind_speed', 'meanpressure', 'month', 'day_of_month']
    FEATURES_TO_NORMALIZE = ['meantemp', 'humidity', 'wind_speed', 'meanpressure']
    FORECAST_DAYS = 7
    PAST_DAYS_WINDOW = 30


def replace_outbounds(df, col_name, lower_value=950, higher_value=1300):
    for i in range(1, len(df) - 1):
        value = df.loc[i, col_name]
        if value < lower_value or value > higher_value:
            prev_value = df.loc[i - 1, col_name]
            j = i + 1
            while df.loc[j, col_name] < lower_value or df.loc[j, col_name] > higher_value and j < len(df):
                j = j + 1
            next_value = prev_value if j == len(df) else df.loc[j, col_name]
            media = (prev_value + next_value) / 2
            for k in range(i, j):
                df.loc[k, col_name] = media
    return df


def data_transformation_layers_initialization(dataframe, features_to_normalize=None):
    # Extract the unique month string values from the month column
    vocabulary = dataframe['date'].dt.month_name().unique()

    # Define the StringLookup layer
    string_lookup = StringLookup(vocabulary=vocabulary,
                                 output_mode='one_hot',
                                 num_oov_indices=0)
    # Define the Keras Normalization layer
    normalizer = Normalization(axis=-1)

    # Extract the names of the numerical features for further normalization
    if features_to_normalize is None:
        features_to_normalize = G.FEATURES_TO_NORMALIZE

    # Train the Normalization layer on the training data features
    normalizer.adapt(dataframe[features_to_normalize])

    return string_lookup, normalizer


def data_transformation(dataframe, normalizer, string_lookup):
    month_feature = string_lookup(dataframe['date'].dt.month_name())
    day_of_month_feature = tf.convert_to_tensor(dataframe["date"].dt.day.to_numpy(), dtype=float)
    day_of_month_feature = tf.expand_dims(day_of_month_feature, axis=1)

    normalized_features = normalizer(dataframe[G.FEATURES_TO_NORMALIZE])
    features = tf.concat([normalized_features, month_feature, day_of_month_feature], axis=1)

    return features


# Create sequences for LSTM
def create_sequences(data, prev_days=None, forecast_days=None):
    if prev_days is None:
        prev_days = G.PAST_DAYS_WINDOW
    if forecast_days is None:
        forecast_days = G.FORECAST_DAYS
    seq_length = prev_days + forecast_days
    sequences = []
    for i in range(len(data) - seq_length):
        sequences.append(data[i:i + seq_length])
    return tf.convert_to_tensor(sequences)


def get_features_and_labels(sequences, prev_days=None):
    if prev_days is None:
        prev_days = G.PAST_DAYS_WINDOW
    sequences_features = sequences[:, :prev_days, :]
    sequences_labels = [sequences[:, prev_days:, 0], sequences[:, prev_days:, 1],
                        sequences[:, prev_days:, 2], sequences[:, prev_days:, 3]]
    return sequences_features, sequences_labels


def create_multioutput_model(num_outputs, prev_days=None, forecast_days=None):
    if prev_days is None:
        prev_days = G.PAST_DAYS_WINDOW
    if forecast_days is None:
        forecast_days = G.FORECAST_DAYS
    input = tf.keras.Input(shape=(prev_days, train_sequences_features.shape[2]))
    outputs = []
    for _ in range(num_outputs):
        x = LSTM(units=64, return_sequences=True)(input)
        x = LSTM(units=32)(x)
        output = Dense(units=forecast_days)(x)
        outputs.append(output)

    model = tf.keras.Model(inputs=input, outputs=outputs)
    model.compile(loss="mse", optimizer="adam")

    return model


def denormalize_data(data, normalizer, index, day=0):
    mean = normalizer.mean[0, index]
    variance = normalizer.variance[0, index]

    original_predictions = data * tf.sqrt(variance + 1e-7) + mean
    next_day_prediction = original_predictions[:, day]

    return next_day_prediction


def plot_graph(dates, real_data, data_column_name, prediction):
    data_column = real_data[data_column_name]

    plt.figure()

    # Plot Pandas data
    plt.plot(dates, data_column, label="Real data")

    # Plot NumPy array
    plt.plot(dates, prediction, label="Next day prediction")

    # Add labels, title, and legend
    plt.xlabel("Date")
    plt.ylabel("Value")
    plt.title("Comparison of values between real data and deep learning prediction")
    plt.legend()

    plt.show()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # Define the path to your CSV file
    data_path = "data/DailyDelhiClimateTrain.csv"

    # Read the CSV file into a pandas DataFrame
    df = pd.read_csv(data_path)

    # Parse the 'date' column to datetime format
    df['date'] = pd.to_datetime(df['date'])

    # Check if parsing was successful (no errors)
    if df['date'].isnull().any():
        print("Warning: Some dates could not be parsed. Please investigate!")

    df = replace_outbounds(df, 'meanpressure')

    # Define train and test split dates
    train_end_date = df["date"].max() - pd.Timedelta(days=10 * (G.PAST_DAYS_WINDOW + G.FORECAST_DAYS))
    train_data = df[df["date"] <= train_end_date]
    test_data = df[df["date"] > train_end_date]

    string_lookup, normalizer = data_transformation_layers_initialization(df)

    full_data = data_transformation(df, normalizer, string_lookup)
    train_data = data_transformation(train_data, normalizer, string_lookup)
    test_data = data_transformation(test_data, normalizer, string_lookup)

    train_sequences = create_sequences(train_data)
    train_sequences = tf.random.shuffle(train_sequences)

    test_sequences = create_sequences(test_data)
    full_sequences = create_sequences(full_data)

    train_sequences_features, train_sequences_labels = get_features_and_labels(train_sequences)
    test_sequences_features, test_sequences_labels = get_features_and_labels(test_sequences)
    full_sequences_features, full_sequences_labels = get_features_and_labels(full_sequences)

    model = create_multioutput_model(4)
    model.summary()

    model.fit(train_sequences_features, train_sequences_labels,
              validation_data=(test_sequences_features, test_sequences_labels),
              epochs=10, batch_size=32)

    # Predict on test data
    predicted_sequences = model.predict(full_sequences_features)
    print(predicted_sequences)

    day = 6
    next_day_predictions = []
    for i in range(4):
        next_day_prediction = denormalize_data(predicted_sequences[i], normalizer, i, day)
        next_day_predictions.append(next_day_prediction)

    print(next_day_predictions)

    # Skip the first month based on Pandas indexing
    filtered_df = df.iloc[30 + day:30 + day + next_day_predictions[0].shape[0]]

    # Extract date and data for plotting
    dates = filtered_df["date"]

    plot_graph(dates, filtered_df, "meantemp", next_day_predictions[0])
    plot_graph(dates, filtered_df, "humidity", next_day_predictions[1])
    plot_graph(dates, filtered_df, "wind_speed", next_day_predictions[2])
    plot_graph(dates, filtered_df, "meanpressure", next_day_predictions[3])
