import tensorflow as tf
import os
import random
from shutil import move
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications.inception_v3 import InceptionV3


class G:
    DATASET_PATH = "data/flower_photos"
    IMAGES_PATH = "data/floral_diversity"
    TRAINING_DIRECTORY = "training"
    VALIDATION_DIRECTORY = "validation"
    IMAGE_SIZE = (150, 150)
    BATCH_SIZE = 32
    VALIDATION_SPLIT = 0.1
    NUM_EPOCHS = 15


def create_train_val_dirs(source_path=G.DATASET_PATH, destination_path=G.IMAGES_PATH):
    groups = [G.TRAINING_DIRECTORY, G.VALIDATION_DIRECTORY]
    labels = [name for name in os.listdir(source_path) if os.path.isdir(os.path.join(source_path, name))]
    labels_paths = {}
    for label in labels:
        labels_paths[label] = {}
        labels_paths[label]['source'] = os.path.join(source_path, label)
        for group in groups:
            label_path = os.path.join(group, label)
            full_path = os.path.join(destination_path, label_path)
            labels_paths[label][group] = full_path
            os.makedirs(full_path)

    for label in labels:
        split_directory_data(labels_paths[label]['source'], labels_paths[label]['training'],
                             labels_paths[label]['validation'])


def split_directory_data(source_dir, training_dir, validation_dir, split_size=G.VALIDATION_SPLIT):
    images_list = os.listdir(source_dir)
    images_list = random.sample(images_list, len(images_list))
    sources = []
    images = []
    for image in images_list:
        image_path = os.path.join(source_dir, image)
        if os.path.getsize(image_path) == 0:
            print("filename is zero length, so ignoring.")
            continue
        images.append(image)
        sources.append(image_path)
    n_images = len(images)
    split_idx = round(n_images * split_size)
    for i in range(n_images):
        if i < split_idx:
            move(sources[i], os.path.join(validation_dir, images[i]))
        else:
            move(sources[i], os.path.join(training_dir, images[i]))


def train_val_generators(training_dir=os.path.join(G.IMAGES_PATH, G.TRAINING_DIRECTORY),
                         validation_dir=os.path.join(G.IMAGES_PATH, G.VALIDATION_DIRECTORY)):
    # Instantiate the ImageDataGenerator class (don't forget to set the arguments to augment the images)
    train_datagen = ImageDataGenerator(rescale=1.0 / 255.0,
                                       rotation_range=45,
                                       width_shift_range=0.2,
                                       height_shift_range=0.2,
                                       shear_range=0.2,
                                       zoom_range=0.2,
                                       horizontal_flip=True,
                                       fill_mode='nearest')

    # Pass in the appropriate arguments to the flow_from_directory method
    train_generator = train_datagen.flow_from_directory(directory=training_dir,
                                                        batch_size=G.BATCH_SIZE,
                                                        class_mode='sparse',
                                                        target_size=G.IMAGE_SIZE)

    # Instantiate the ImageDataGenerator class (don't forget to set the rescale argument)
    validation_datagen = ImageDataGenerator(rescale=1.0 / 255.0)

    # Pass in the appropriate arguments to the flow_from_directory method
    validation_generator = validation_datagen.flow_from_directory(directory=validation_dir,
                                                                  batch_size=G.BATCH_SIZE,
                                                                  class_mode='sparse',
                                                                  target_size=G.IMAGE_SIZE)
    return train_generator, validation_generator


def load_inception_model(input_shape=(G.IMAGE_SIZE[0], G.IMAGE_SIZE[1], 3), last_desired_layer_name='mixed7',
                         trainable=False):
    inception_model = InceptionV3(input_shape=input_shape, include_top=False)
    inception_model.trainable = trainable
    last_desired_layer = inception_model.get_layer(last_desired_layer_name)
    return inception_model.input, last_desired_layer.output


def create_model(num_classes, use_inception=False):
    input_shape = (G.IMAGE_SIZE[0], G.IMAGE_SIZE[1], 3)

    if use_inception is True:
        input, conv = load_inception_model()
    else:
        input = tf.keras.Input(shape=input_shape)
        conv = tf.keras.layers.Conv2D(16, 3, activation='relu')(input)
        conv = tf.keras.layers.MaxPooling2D(2, 2)(conv)
        conv = tf.keras.layers.Conv2D(32, 3, activation='relu')(conv)
        conv = tf.keras.layers.MaxPooling2D(2, 2)(conv)
        conv = tf.keras.layers.Conv2D(64, 3, activation='relu')(conv)
        conv = tf.keras.layers.MaxPooling2D(2, 2)(conv)

    conv = tf.keras.layers.Flatten()(conv)

    conv = tf.keras.layers.Dense(512, activation='relu')(conv)
    output = tf.keras.layers.Dense(num_classes, activation='softmax')(conv)

    model = tf.keras.Model(inputs=[input], outputs=output)

    model.compile(loss='sparse_categorical_crossentropy',
                  optimizer='RMSprop',
                  metrics=['accuracy'])

    return model


if __name__ == '__main__':
    # Not needed anymore as I already moved all the images to train and validation
    # create_train_val_dirs()
    label_names = [name for name in os.listdir(os.path.join(G.IMAGES_PATH, G.TRAINING_DIRECTORY)) if
              os.path.isdir(os.path.join(os.path.join(G.IMAGES_PATH, G.TRAINING_DIRECTORY), name))]

    train_generator, validation_generator = train_val_generators()

    model = create_model(len(label_names), True)
    model.summary()

    callback = tf.keras.callbacks.EarlyStopping(patience=3)
    history = model.fit(train_generator,
                        epochs=G.NUM_EPOCHS,
                        verbose=1,
                        validation_data=validation_generator,
                        callbacks=[callback])
