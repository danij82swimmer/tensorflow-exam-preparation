import tensorflow as tf
import pandas as pd
from sklearn.model_selection import train_test_split


class G:
    EPOCHS = 10
    VALIDATION_PERCENTAGE = 0.2
    DATASET_FILE = 'data/citrus.csv'
    FEATURE_COLUMNS = ['diameter', 'weight', 'red', 'green', 'blue']
    LABEL_COLUMN = 'isOrange'
    FEATURES_TO_NORMALIZE = ['diameter', 'weight']


def get_normalization_layers(ds, features_to_normalize=None):
    if features_to_normalize is None:
        features_to_normalize = G.FEATURES_TO_NORMALIZE
    normalization_layers = {}
    for key in features_to_normalize:
        normalization_layer = tf.keras.layers.Normalization(axis=None)
        normalization_layer.adapt(ds[key])
        normalization_layers[key] = normalization_layer

    return normalization_layers


def generate_model(normalization_layers, features=None):
    if features is None:
        features = G.FEATURE_COLUMNS

    input_list = []
    list_after_normalization = []
    for key in features:
        input = tf.keras.Input(shape=(1,), name=key)
        input_list.append(input)
        if key in normalization_layers:
            normalized = normalization_layers[key](input)
            list_after_normalization.append(normalized)
        else:
            list_after_normalization.append(input)

    combined = tf.keras.layers.concatenate(list_after_normalization, 1)
    l1 = tf.keras.layers.Dense(8, activation='relu')(combined)
    l2 = tf.keras.layers.Dense(8, activation='relu')(l1)
    output = tf.keras.layers.Dense(1, activation='sigmoid')(l2)
    model = tf.keras.Model(inputs=input_list, outputs=output)

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics='accuracy')
    return model


if __name__ == '__main__':
    # Load training and test data
    df = pd.read_csv(G.DATASET_FILE)

    # Creating the label. orange = 1 and grapefruit = 0
    df[G.LABEL_COLUMN] = 0
    df.loc[df['name'] == 'orange', G.LABEL_COLUMN] = 1
    df.drop(['name'], axis=1, inplace=True)

    # Rescale the colors
    df['red'] = df['red'] / 255.
    df['green'] = df['green'] / 255.
    df['blue'] = df['blue'] / 255.

    # Split the dataframe into train and test sets
    X_train, X_test, y_train, y_test = train_test_split(
        df[G.FEATURE_COLUMNS],  # Features
        df[G.LABEL_COLUMN],  # Labels
        test_size=G.VALIDATION_PERCENTAGE,
        random_state=42  # Set a random seed for reproducibility
    )

    normalization_layers = get_normalization_layers(X_train)

    model = generate_model(normalization_layers)
    model.summary()

    history = model.fit(dict(X_train), y_train, epochs=G.EPOCHS, validation_data=(dict(X_test), y_test))
