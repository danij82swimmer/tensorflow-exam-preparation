import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array

# Read the CSV file
train_data = pd.read_csv("data/plantPathology/info.csv")

# Define image paths
train_image_filenames = [f"{image_id}.jpg" for image_id in train_data["image_id"]]
train_data['image_path'] = [f"data/plantPathology/images/{image_id}.jpg" for image_id in train_data["image_id"]]

# Define images folder
source_folder = "data/plantPathology/images"

# Add the train image paths into an array
train_image_paths = []
for image_filename in train_image_filenames:
    source_path = os.path.join(source_folder, image_filename)
    train_image_paths.append(source_path)

# Extract the labels into a numpy array
healthy_labels = train_data["healthy"]
multiple_diseases_labels = train_data["multiple_diseases"]
rust_labels = train_data["rust"]
scab_labels = train_data["scab"]
labels = np.array([healthy_labels,
                   multiple_diseases_labels,
                   rust_labels, scab_labels]).T

# Load images into a numpy array
def load_images(train_image_paths):
    train_images = []
    for image_path in train_image_paths:
        image = load_img(image_path, target_size=(224, 224))
        image = img_to_array(image)
        train_images.append(image)
    train_images = np.array(train_images)
    return train_images

# Define the size of the training and validation sets
num_train_samples = int(len(train_images) * 0.9)
num_val_samples = len(train_images) - num_train_samples

# Split the images and labels into training and validation sets
X_train, X_test, y_train, y_test = train_test_split(train_data['image_path'],
                                                    train_data[['healthy', 'multiple_diseases', 'rust', 'scab']],
                                                    test_size=G.VALIDATION_PERCENTAGE,
                                                    random_state=42)
X_train = load_images(X_train)
X_test = load_images(X_test)
train_images, val_images = train_images[:num_train_samples], train_images[num_train_samples:]
train_labels, val_labels = labels[:num_train_samples], labels[num_train_samples:]

# Data augmentation for improved training
train_datagen = ImageDataGenerator(rescale=1. / 255,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True)
train_generator = train_datagen.flow(train_images, train_labels, batch_size=32)
train_generator = train_datagen.flow(X_train, y_train, batch_size=32)

# Data generator for validation
val_datagen = ImageDataGenerator(rescale=1. / 255)
val_generator = val_datagen.flow(val_images, val_labels, batch_size=32)
val_generator = val_datagen.flow(X_test, y_test, batch_size=32)

# Define the EarlyStopping callback
early_stopping = EarlyStopping(monitor="val_accuracy", patience=3)

# Define the model architecture
model = Sequential([
    Conv2D(32, (3, 3), activation="relu", input_shape=(224, 224, 3)),
    MaxPooling2D((2, 2)),
    Conv2D(64, (3, 3), activation="relu"),
    MaxPooling2D((2, 2)),
    Flatten(),
    Dense(128, activation="relu"),
    Dense(4, activation="sigmoid")  # 4 outputs for healthy, multiple_diseases, rust, scab
])

# Compile the model
model.compile(loss="binary_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])

# Train the model and store the history
history = model.fit(train_generator,
                    validation_data=val_generator,
                    epochs=150,
                    callbacks=[early_stopping])

# Extract the loss and accuracy values from the history
train_loss = history.history["loss"]
train_accuracy = history.history["accuracy"]
val_loss = history.history["val_loss"]
val_accuracy = history.history["val_accuracy"]

# Plot the loss and accuracy curves for training and validation
plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.plot(train_loss, label="Training Loss")
plt.plot(val_loss, label="Validation Loss")
plt.title("Loss")
plt.xlabel("Epochs")
plt.ylabel("Loss")
plt.legend()

plt.subplot(2, 1, 2)
plt.plot(train_accuracy, label="Training Accuracy")
plt.plot(val_accuracy, label="Validation Accuracy")
plt.title("Accuracy")
plt.xlabel("Epochs")
plt.ylabel("Accuracy")
plt.legend()
plt.show()

# Predict probabilities for test
test_ids = pd.read_csv("kaggle_data/test.csv")
test_filenames = test_ids['image_id'] + '.jpg'

test_destination_folder = "kaggle_data/images/test"
if not os.path.exists(test_destination_folder):
    os.makedirs(test_destination_folder)

test_image_paths = []
# Move the images
for image_path in test_filenames:
    source_path = os.path.join(source_folder, image_path)
    destination_path = os.path.join(test_destination_folder, image_path)
    os.rename(source_path, destination_path)
    test_image_paths.append(destination_path)

test_images = []
for image_path in test_image_paths:
    image = load_img(image_path, target_size=(224, 224))
    image = img_to_array(image)
    test_images.append(image)
test_images = np.array(test_images)

test_image_data = train_datagen.flow(test_images, shuffle=False, batch_size=1)
predictions = model.predict(test_image_data)

# Print the predicted probabilities
for i, image in enumerate(test_ids['image_id']):
    print(f"Prediction for image {image}:")
    print(f"\tHealthy: {predictions[i][0]:0.3f}")
    print(f"\tMultiple Diseases: {predictions[i][1]:0.3f}")
    print(f"\tRust: {predictions[i][2]:0.3f}")
    print(f"\tScab: {predictions[i][3]:0.3f}")

# Evaluate the model on the training data
loss, accuracy = model.evaluate(train_generator)
print(f"Accuracy on training data: {accuracy}")
