import tensorflow as tf
import numpy as np
import string


class G:
    TEXT_PATH = "data"
    FILE_PATH = "data/romeo-and-juliet.txt"
    BATCH_SIZE = 32
    VALIDATION_SPLIT = 0.1


def get_text_vectorization_layer(max_len):
    layer = tf.keras.layers.TextVectorization(output_sequence_length=max_len)
    return layer


def change_padding(sequences):
    new_sequences = []
    for sequence in sequences:
        n_words = np.count_nonzero(sequence)
        new_sequence = np.concatenate((sequence[n_words:], sequence[:n_words]))
        new_sequence = np.reshape(new_sequence, sequence.shape)
        new_sequences.append(new_sequence)
    new_sequences = np.array(new_sequences, dtype=np.int64)
    return new_sequences


def create_model(total_words, max_sequence_len):
    # Hyperparameters
    embedding_dim = 100
    lstm_units = 150
    learning_rate = 0.01

    # Build the model
    model = tf.keras.Sequential([
        tf.keras.layers.Embedding(total_words, embedding_dim, input_length=max_sequence_len - 1),
        tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(lstm_units)),
        tf.keras.layers.Dense(total_words, activation='softmax')
    ])

    # Use categorical crossentropy because this is a multi-class problem
    model.compile(
        loss='sparse_categorical_crossentropy',
        optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate),
        metrics=['accuracy']
    )

    # Print the model summary
    model.summary()
    return model


if __name__ == '__main__':
    with open(G.FILE_PATH) as f:
        lines = f.readlines()
    sentences = []
    splitted_sentences = []
    maxlen = 0
    for line in lines:
        line = line.translate(str.maketrans('', '', string.punctuation)).lower().replace('\n', '')
        line = line.rstrip()
        line = line.lstrip()
        line_splitted = line.split()
        maxlen = max(maxlen, len(line_splitted))
        sentences.append(" ".join(line_splitted))
        splitted_sentences.append(line_splitted)

    tokenizer = get_text_vectorization_layer(maxlen)
    tokenizer.adapt(sentences)

    input_sequences = []
    for line in splitted_sentences:
        len_line = len(line)
        for i in range(2, len_line + 1):
            sentence = " ".join(line[:i])
            input_sequences.append(sentence)

    transformed_sentences = change_padding(tokenizer(input_sequences).numpy())

    features = transformed_sentences[:, :-1]
    labels = transformed_sentences[:, -1]

    ds_train = tf.data.Dataset.from_tensor_slices((features, labels)).batch(32)

    model = create_model(tokenizer.vocabulary_size(), maxlen)

    epochs = 100

    # Train the model
    history = model.fit(ds_train, epochs=epochs)

    # Define seed text
    seed_text = "Juliet"

    vocabulary = tokenizer.get_vocabulary()

    # Define total words to predict
    next_words = 8

    # Loop until desired length is reached
    for _ in range(next_words):

        # Convert the seed text to a token sequence
        transformed_test = change_padding(tokenizer([seed_text]).numpy())[:, 1:]

        print('transformed text = ', transformed_test)

        # Feed to the model and get the probabilities for each index
        probabilities = model.predict(transformed_test)

        # Pick a random number from [1,2,3]
        choice = np.random.choice([1, 2, 3])

        # Sort the probabilities in ascending order
        # and get the random choice from the end of the array
        next_word_predicted = np.argsort(probabilities)[0][-choice]

        # Ignore if index is 0 because that is just the padding.
        if next_word_predicted != 0:
            # Look up the word associated with the index.
            output_word = vocabulary[next_word_predicted]

            # Combine with the seed text
            seed_text += " " + output_word

    # Print the result
    print(seed_text)
