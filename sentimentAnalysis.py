import nltk
import tensorflow as tf
import pandas as pd
import numpy as np
import re

from sklearn.model_selection import train_test_split
from nltk.corpus import stopwords
from matplotlib import pyplot as plt

nltk.download('punkt')
# download stopwords
nltk.download("stopwords")
# obtain additional stopwords from nltk
stop_words = stopwords.words('english')
stop_words.extend(['from', 'subject', 're', 'edu', 'use'])


class G:
    BATCH_SIZE = 64
    EPOCHS = 10  # 4 not pretrained
    MAX_LEN = 200
    EMBEDDINGS_DIM = 100
    VALIDATION_PERCENTAGE = 0.5
    MAX_WORDS = 300
    EMBEDDING_FILE = 'data/glove.6B.100d.txt'


def preprocess_string(string):
    remove_regex = r'[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']'
    s = tf.strings.regex_replace(string, remove_regex, '')

    # remove stock market tickers like $GE
    s = tf.strings.regex_replace(s, b'\$\w*', b'')
    # remove old style retweet text "RT"
    s = tf.strings.regex_replace(s, b'^RT[\s]+', b'')

    # lower case
    s = tf.strings.lower(s)
    # remove digits
    s = tf.strings.regex_replace(s, b'[0-9]+', b'')
    # remove hyperlinks
    s = tf.strings.regex_replace(s, b'https?://[^\s\n\r]+', b'')

    return s


# Define a preprocessing function
def preprocess_text(text):
    # Convert text to lowercase
    text = text.lower()

    # Remove punctuation and special characters
    text = re.sub(r'[^\w\s]', '', text)

    # Tokenize the text into words
    words = nltk.word_tokenize(text)

    # Remove stop words
    stop_words = nltk.corpus.stopwords.words('english')
    words = [word for word in words if word not in stop_words]

    # Join the words back into a preprocessed text string
    preprocessed_text = ' '.join(words)

    return preprocessed_text


def adapt_precompute_layers(dataset):
    vectorizer = tf.keras.layers.TextVectorization(max_tokens=G.MAX_WORDS+2,
                                                   output_sequence_length=G.MAX_LEN,
                                                   standardize=preprocess_string)
    vectorizer.adapt(dataset)

    return vectorizer


def load_embeddings(vectorizer):
    embeddings_index = {}
    with open(G.EMBEDDING_FILE, encoding="utf-8") as f:
        for line in f:
            word, coefs = line.split(maxsplit=1)
            coefs = np.fromstring(coefs, "f", sep=" ")
            embeddings_index[word] = coefs

    print("Found %s word vectors." % len(embeddings_index))

    voc = vectorizer.get_vocabulary()
    word_index = dict(zip(voc, range(len(voc))))

    num_tokens = len(voc)
    hits = 0
    misses = 0

    # Prepare embedding matrix
    embedding_matrix = np.zeros((num_tokens, G.EMBEDDINGS_DIM))
    for word, i in word_index.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            # Words not found in embedding index will be all-zeros.
            # This includes the representation for "padding" and "OOV"
            embedding_matrix[i] = embedding_vector
            hits += 1
        else:
            misses += 1
    print("Converted %d words (%d misses)" % (hits, misses))

    return embedding_matrix, num_tokens


def create_model(vectorizer, embedding_matrix, num_tokens):
    model_input = tf.keras.layers.Input(shape=(1,), dtype=tf.string)

    vectors = vectorizer(model_input)
    embeddings = tf.keras.layers.Embedding(num_tokens,
                                           G.EMBEDDINGS_DIM,
                                           weights=[embedding_matrix],
                                           trainable=True)(vectors)
    x = tf.keras.layers.Conv1D(filters=G.EMBEDDINGS_DIM, kernel_size=5,
                               strides=1, padding="causal",
                               activation="relu")(embeddings)
    x = tf.keras.layers.Dropout(0.5)(x)
    x = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(G.EMBEDDINGS_DIM, return_sequences=True))(x)
    x = tf.keras.layers.Dropout(0.5)(x)
    x = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(G.EMBEDDINGS_DIM))(x)
    x = tf.keras.layers.Dropout(0.5)(x)
    x = tf.keras.layers.Dense(32, activation="relu")(x)
    x = tf.keras.layers.Dropout(0.5)(x)
    x = tf.keras.layers.Dense(16, activation="relu")(x)
    output = tf.keras.layers.Dense(3, activation='softmax')(x)

    final_model = tf.keras.Model(inputs=[model_input], outputs=output)
    optimizer = tf.keras.optimizers.SGD(learning_rate=1e-5, momentum=0.9)
    final_model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    final_model.summary()

    return final_model


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Load the data
    col_names = ['sentiment', 'ids', 'date', 'flag', 'user', 'text']
    df_reviews = pd.read_csv("data/sentiment140.csv",
                             encoding='latin1',
                             names=col_names,
                             header=None)

    X_train, X_test, y_train, y_test = train_test_split(df_reviews['text'],
                                                        df_reviews['sentiment'],
                                                        test_size=G.VALIDATION_PERCENTAGE,
                                                        random_state=42)
    y_train = y_train / 4

    print('adapting tokenizer')
    vectorizer = adapt_precompute_layers(X_train)
    print('initializing embeddings')
    embedding_matrix, num_tokens = load_embeddings(vectorizer)
    print('creating model')
    model = create_model(vectorizer, embedding_matrix, num_tokens)

    print('starting training')
    history = model.fit(X_train,
                        y_train,
                        validation_data=(X_test, y_test),
                        batch_size=G.BATCH_SIZE, epochs=G.EPOCHS)

    print('showing graphs')
    pd.DataFrame(history.history).plot(figsize=(8, 5))
    plt.show()
