import tensorflow as tf
import json
from sklearn.model_selection import train_test_split


def extract_vocabularies(dataframe):
    ingredients_vocab = set()
    max_ingredients_in_recipe = 1
    cuisines_vocab = set()
    for recipe in dataframe:
        num_ingredients = len(recipe["ingredients"])
        max_ingredients_in_recipe = max_ingredients_in_recipe if max_ingredients_in_recipe >= num_ingredients else num_ingredients
        for ingredient in recipe["ingredients"]:
            # Replacing whitespaces, so we can pass a whole sentence with the list of ingredients later on to the
            # TextVectorization layer
            ingredients_vocab.add(ingredient.replace(" ", "_"))
        cuisines_vocab.add(recipe["cuisine"])

    ingredients_vocab = list(ingredients_vocab)
    cuisines_vocab = list(cuisines_vocab)
    return ingredients_vocab, max_ingredients_in_recipe, cuisines_vocab


def initialize_tokenization(ingredients_vocab, max_ingredients_in_recipe, cuisines_vocab):
    ingredients_lookup = tf.keras.layers.TextVectorization(
        standardize=None,
        vocabulary=ingredients_vocab,
        output_mode="int",
        output_sequence_length=max_ingredients_in_recipe
    )
    cuisines_lookup = tf.keras.layers.StringLookup(
        vocabulary=cuisines_vocab, output_mode="int"
    )

    return ingredients_lookup, cuisines_lookup


# Extract features and labels from dataframe
def extract_features_and_labels(dataframe, ingredients_lookup, cuisines_lookup):
    features = []
    labels = []
    for recipe in dataframe:
        # Joining the list of ingredients in a single string
        ingredients = []
        for ingredient in recipe["ingredients"]:
            ingredients.append(ingredient.replace(" ", "_"))
        features.append(" ".join(ingredients))
        labels.append(recipe["cuisine"])

    features = ingredients_lookup(features)
    labels = cuisines_lookup(labels)
    labels = tf.expand_dims(labels, axis=1)
    return features, labels


# Define model architecture
def create_model(number_of_ingredients, max_ingredients_in_recipe, number_of_cuisines):
    input = tf.keras.Input(shape=(max_ingredients_in_recipe))
    # Embedding layer for ingredients
    x = tf.keras.layers.Embedding(number_of_ingredients, 64)(input)
    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.Dense(32, activation="relu")(x)
    output = tf.keras.layers.Dense(number_of_cuisines, activation="softmax")(x)

    model = tf.keras.Model(inputs=input, outputs=output)
    model.compile(loss="sparse_categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

    return model


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # Load training and test data
    with open("data/recipes.json") as f:
        recipes = json.load(f)

    # Define your desired test group percentage
    test_size = 0.1

    # Perform the train-test split
    train_recipes, test_recipes = train_test_split(recipes, test_size=test_size)

    # Extract ingredient and cuisine vocabularies
    train_ingredients_vocab, max_ingredients_in_recipe, train_cuisines_vocab = extract_vocabularies(train_recipes)

    # Create tokenization layers for encoding
    ingredients_lookup, cuisines_lookup = initialize_tokenization(train_ingredients_vocab,
                                                                  max_ingredients_in_recipe,
                                                                  train_cuisines_vocab)

    number_of_ingredients = len(ingredients_lookup.get_vocabulary())
    number_of_cuisines = len(cuisines_lookup.get_vocabulary())

    train_features, train_labels = extract_features_and_labels(train_recipes, ingredients_lookup, cuisines_lookup)
    test_features, test_labels = extract_features_and_labels(test_recipes, ingredients_lookup, cuisines_lookup)

    model = create_model(number_of_ingredients, max_ingredients_in_recipe, number_of_cuisines)
    model.summary()

    # Train the model
    model.fit(train_features,
              train_labels,
              epochs=10,
              batch_size=128,
              validation_data=(test_features, test_labels))
